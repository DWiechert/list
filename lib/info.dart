import 'dart:core';
import 'dart:io';

import 'package:path/path.dart';

class FileInfo {
  String name;
  bool isDirectory;
  num size;

  @override
  String toString() => '$name\t$isDirectory\t$size';
}

List<FileInfo> getFileInfo(FileSystemEntity entity, [bool all = true]) {
  final info = FileInfo();
  info.name = basename(entity.path);
  info.isDirectory = (entity is Directory) ? true : false;
  info.size = entity.statSync().size;

  final list = List<FileInfo>();
  list.add(info);

  if (info.isDirectory && all) {
    final children = (entity as Directory).listSync();
    children.forEach((c) => list.addAll(getFileInfo(c, false)));
  }

  return list;
}

void printFileInfos(List<FileInfo> infos) {
  var s = 'Name\tIsDirectory\tSize';
  var s2 = infos.map((i) => i.toString()).toList();
  final output = s + '\n' + s2.join('\n');
  print(output);
}